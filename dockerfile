# Используем базовый образ nginx-unprivileged
FROM nginxinc/nginx-unprivileged

COPY ./index.html /usr/share/nginx/html/index.html

# Label build
ARG GIT_REPO
ARG GIT_COMMIT
ARG GIT_DATE
ARG IMG_NAME

# Устанавливаем значения аргументов в переменные среды
ENV GIT_REPO=$GIT_REPO \
    GIT_COMMIT=$GIT_COMMIT \
    GIT_DATE=$GIT_DATE \
    IMG_NAME=$IMG_NAME

# Устанавливаем метки для образа
LABEL git.repo="$GIT_REPO" \
      git.commit="$GIT_COMMIT" \
      git.date="$GIT_DATE"
